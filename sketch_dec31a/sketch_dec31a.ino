#include <LiquidCrystal.h>
#include <Keypad.h>
#include <EEPROM.h>

//Параметры дисплея
const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Параметры клавиатуры
const byte ROWS       = 3;
const byte COLS       = 4;
byte rowPins[ROWS]    = {8, A0, A1}; 
byte colPins[COLS]    = {A2, A3, A4, A5}; 
char keys[ROWS][COLS] = {{'<','>','X','S'},
                         {'A','B','C','D'},
                         {'+','-','n','n'}};
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

//Служебные переменные
byte editPresetFlag          = 0;
byte currentBankNumber       = 0;
byte currentPresetNumber     = 0;
byte newPresetValue          = 0;
byte currentPresetValue      = 0;
byte currentPositionChanged  = 0;
const byte BANKS             = 9;
const byte PRESETS           = 4;
byte presets[BANKS][PRESETS] = {{0, 0, 0, 0},
                                {0, 0, 0, 0},
                                {0, 0, 0, 0},
                                {0, 0, 0, 0},
                                {0, 0, 0, 0},
                                {0, 0, 0, 0},
                                {0, 0, 0, 0},
                                {0, 0, 0, 0},
                                {0, 0, 0, 0}};


void setup()
{
  //----Карта EEPROM:
  //  первые 2 байта - последний активный банк и последний активный пресет
  //  далее последовательно - каждые 4 байта на банк: 1 банк (пресет1, пресет2 и т.д.)

  //Изначально все реле в нуле
  relay(0x00);
  
  //Проверка первого включения
  if (EEPROM.read(0x03 + BANKS * PRESETS) == 0xFF)
  {
    //Сохраняем кругом нули
    for (byte i = 0; i <= 0x03 + BANKS * PRESETS; i++)
      EEPROM.write(i, 0x00);
  }
  
  //Загрузка последнего активного пресета с EEPROM
  currentBankNumber    = EEPROM.read(0x00);
  currentPresetNumber  = EEPROM.read(0x01);
  //Загрузка пресетов
  byte address = 0x02;
  for (byte i = 0; i < BANKS; i++)
  {
    for (byte j = 0; j < PRESETS; j++)
    {
      presets[i][j] = EEPROM.read(address);
      address += 1;
    }
  }
  //Получаем текущий пресет
  currentPresetValue = presets[currentBankNumber][currentPresetNumber];
  
  //Переключаем реле  и светодиоды
  relay(currentPresetValue);
  
  //Инициализация размеров экрана
  lcd.begin(16, 2);
  //Подготавливаем выводимую строку
  char string[17];
  sprintf(string, "BANK:%d  PRESET:%d", currentBankNumber + 1, currentPresetNumber + 1);
  lcd.clear();
  lcd.print(string);
  lcd.setCursor(0, 1);
  loopStateString(string, currentPresetValue, 0x00);
  lcd.print(string);
}

void loop()
{
  char key = keypad.getKey();
  switch(key)
  {
    //----КНОПКИ МЕНЮ------------------------------------------------------------------------------
    //----Кнопки выбора петли [Влево][Вправо] при редактировании пресета
    case '<':
    case '>':
    {
      if(editPresetFlag == 0x01)
      {
        if (key == '<')
        {
          currentPositionChanged += 1;
          if (currentPositionChanged > 7)
            currentPositionChanged = 0;
        }
        else if (key == '>')
        {
          if (currentPositionChanged == 1)
            currentPositionChanged = 0;
          else if (currentPositionChanged == 0)
            currentPositionChanged = 7;
          else currentPositionChanged -= 1;
        }
        
        //Выводим курсор в новой позиции
        if (currentPositionChanged < 4)
          lcd.setCursor(11 - currentPositionChanged, 1);
        else lcd.setCursor(8 - currentPositionChanged, 1);
      }
    }; 
    break;
    //----Кнопка выбора петель [Задействована]/[Незадействована]
    case 'X':
    {
      if(editPresetFlag == 0x01)
      {
        //Инвертируем соответствующий бит в пресете
        currentPresetValue ^= (0x01 << currentPositionChanged);
        //Обновляем информацию на экране
        char string[17];
        lcd.setCursor(0, 1);
        loopStateString(string, currentPresetValue, 0x01);
        lcd.print(string);
        //Возобновляем моргать курсором
        if (currentPositionChanged < 4)
          lcd.setCursor(11 - currentPositionChanged, 1);
        else lcd.setCursor(8 - currentPositionChanged, 1);
        lcd.cursor();
        lcd.blink();
      }
    }; 
    break;
    //----Кнопка [Редактировать]/[Сохранить] пресет 
    case 'S':
    {
      if(editPresetFlag == 0x01)
      {
        //Сохранение пресета в EEPROM
        EEPROM.write(0x02 + currentBankNumber * PRESETS + currentPresetNumber, currentPresetValue);
        //Сохранение в оперативке - в глобальном массиве пресетов
        presets[currentBankNumber][currentPresetNumber] = currentPresetValue;
        //Переключаем реле  и светодиоды
        relay(currentPresetValue);
        
        //Выход из режима редактирования
        editPresetFlag = 0x00;
        lcd.clear();
        char string[17];
        sprintf(string, "BANK:%d  PRESET:%d", currentBankNumber + 1, currentPresetNumber + 1);
        lcd.print(string);
        lcd.setCursor(0, 1);
        loopStateString(string, currentPresetValue, 0x00);
        lcd.print(string);

        //Убираем курсор
        lcd.noBlink();
        lcd.noCursor();
      }
      else
      {
        //Вход в режим редактирования пресета
        editPresetFlag = 0x01;
        //Выводим на экран режим редактирования пресета
        lcd.clear();
        char string[17];
        sprintf(string, " Loop   Main %d/%d", currentBankNumber + 1, currentPresetNumber + 1);
        lcd.print(string);
        lcd.setCursor(0, 1);
        loopStateString(string, currentPresetValue, 0x01);
        lcd.print(string);
        //Начинаем моргать курсором
        if (currentPositionChanged < 4)
          lcd.setCursor(11 - currentPositionChanged, 1);
        else lcd.setCursor(8 - currentPositionChanged, 1);
        lcd.cursor();
        lcd.blink();
      }
    }; 
    break;


    //----КНОПКИ ПЕРЕКЛЮЧЕНИЯ ПРЕСЕТОВ/БАНКОВ------------------------------------------------------
    case 'A':
    case 'B':
    case 'C':
    case 'D':
    case '+':
    case '-':
    {
      //----Кнопки переключения пресетов в текущем банке: [A], [B], [C], [D] 
      //----и смены текущего банка: [+], [-]
      byte tempPresetNumber = currentPresetNumber;
      byte tempBankNumber   = currentBankNumber;
      
      if(editPresetFlag == 0x00)
      {
        if (key == 'A')
          currentPresetNumber = 0;
        else if (key == 'B')
          currentPresetNumber = 1;
        else if (key == 'C')
          currentPresetNumber = 2;
        else if (key == 'D')
          currentPresetNumber = 3;
        else if (key == '+')
        {
          currentBankNumber += 1;
          if (currentBankNumber == BANKS)
            currentBankNumber = 0;
        }
        else if (key == '-')
        {
          if (currentBankNumber == 1)
            currentBankNumber = 0;
          else if (currentBankNumber == 0)
            currentBankNumber = BANKS - 1;
          else currentBankNumber -= 1;
        }

        //Проверяем, что действительно что-то поменялось
        if (tempBankNumber != currentBankNumber || tempPresetNumber != currentPresetNumber)
        {
          //Сохранение в EEPROM текущего активного банка и пресета
          EEPROM.write(0x00, currentBankNumber);
          EEPROM.write(0x01, currentPresetNumber);
          //Выбор из оперативной памяти - глобального массива - выбранного пресета
          currentPresetValue = presets[currentBankNumber][currentPresetNumber];
          //Переключаем реле  и светодиоды
          relay(currentPresetValue);
          
          //Подготавливаем выводимую строку
          char string[17];
          lcd.clear();
          sprintf(string, "BANK:%d  PRESET:%d", currentBankNumber + 1, currentPresetNumber + 1);
          lcd.print(string);
          lcd.setCursor(0, 1);
          loopStateString(string, currentPresetValue, 0x00);
          lcd.print(string);
        }
      }
    }; 
    break;
  }
}


//Функция формирования строки состояния пресета
void loopStateString(char* string, byte currentPresetValue, byte editFlag)
{
  char changeFlag = ' ';
  if (editFlag == 0x01)
  {
    //Находимся в режиме редактирования пресета
    sprintf(string, "<    < <    <  *");
    //Выводим номера уже задействованных петель в блоке коммутации основных эффектов
    for (byte i = 0; i < 4; i++)
    {
      if ((currentPresetValue & (0x01 << i)) == (0x01 << i))
        string[11 - i] = (char)(0x30 + (i + 1));
      else string[11 - i] = '_';
    }
    //Выводим номера уже задействованных петель в блоке коммутации эффектов в петле "головы"
    for (byte i = 4; i < 8; i++)
    {
      if ((currentPresetValue & (0x01 << i)) == (0x01 << i))
        string[8 - i] = (char)(0x30 + (i + 1));
      else string[8 - i] = '_';
    }
  }
  else
  {
    //Штатный режим работы
    sprintf(string, "<    < <    <   ");
    //Выводим номера уже задействованных петель в блоке коммутации основных эффектов
    for (byte i = 0; i < 4; i++)
    {
      if ((currentPresetValue & (0x01 << i)) == (0x01 << i))
        string[11 - i] = (char)0xFF;
      else string[11 - i] = '_';
    }
    //Выводим номера уже задействованных петель в блоке коммутации эффектов в петле "головы"
    for (byte i = 4; i < 8; i++)
    {
      if ((currentPresetValue & (0x01 << i)) == (0x01 << i))
        string[8 - i] = (char)0xFF;
      else string[8 - i] = '_';
    }
  }
}


//Функция управления апаратной частью - реле и светодиодами
void relay(byte presetValue)
{
  //Что-то
}
